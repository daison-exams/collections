<?php

namespace Daison\Pegion\Repositories\Order;

use Daison\Pegion\Entities\Order;
use Daison\Pegion\Repositories\Pegion;
use Daison\Pegion\Repositories\Order\Details;
use Daison\Pegion\Contracts\Order\ListsInterface;

class Lists implements ListsInterface
{
    protected $currentPage = 1;
    protected $recordsPerPage = 10;

    public function __construct($builder)
    {
        $this->builder = $builder;
    }

    /**
     * We need to get the latest builder.
     *
     * This is needed to build the paginator in the laravel.
     *
     * @return mixed
     */
    public function getLatestBuilder()
    {
        return $this->builder;
    }

    /**
     * {@inheritdoc}
     */
    public function handle()
    {
        $records = $this->builder
            ->limit($this->recordsPerPage)
            ->offset($this->recordsPerPage * $this->currentPage)
            ->get();

        return $records->map(function ($record) {
            $details = (new Details($record))->handle();

            return [
                'id' => $details->getId(),
                'from' => $details->getFrom(),
                'distance' => $details->getDistance(),
                'deadline' => $details->getDeadline(),
                'pegion_id' => $details->getPegionId(),
                'pegion' => (new Pegion($record->pegion))->handle(),
                'costs' => $details->getCosts(),
                'starts_at' => $details->getStartsAt(),
                'ends_at' => $details->getEndsAt(),
                'downtime' => $details->getDowntime(),
                'overall_ends_at' => $details->getOverallEndsAt(),
                'created_at' => $details->getCreatedAt(),
                'updated_at' => $details->getUpdatedAt(),
            ];
        })->all();
    }

    /**
     * {@inheritdoc}
     */
    public function sortByAscending($field)
    {
        $this->builder->orderBy($field, 'asc');

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function currentPage($value)
    {
        $this->currentPage = $value;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function recordsPerPage($value)
    {
        $this->recordsPerPage = $value;

        return $this;
    }
}
