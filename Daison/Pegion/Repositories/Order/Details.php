<?php

namespace Daison\Pegion\Repositories\Order;

use Daison\Pegion\Entities\Order;
use Daison\Pegion\Contracts\Order\DetailsInterface;

class Details implements DetailsInterface
{
    protected $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * {@inheritdoc}
     */
    public function handle()
    {
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->order->id;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getFrom()
    {
        return $this->order->from;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getDistance()
    {
        return $this->order->dist;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getDeadline()
    {
        return $this->order->deadline;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getPegionId()
    {
        return $this->order->pegion_id;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getCosts()
    {
        return $this->order->costs;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getStartsAt()
    {
        return $this->order->starts_at;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getEndsAt()
    {
        return $this->order->ends_at;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getDowntime()
    {
        return $this->order->downtime;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getOverallEndsAt()
    {
        return $this->order->overall_ends_at;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getCreatedAt()
    {
        return $this->order->created_at;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getUpdatedAt()
    {
        return $this->order->updated_at;
    }

    /**
     * {@inheritdoc}
     */
    public function setId()
    {
        # do nothing...
    }

    /**
     * {@inheritdoc}
     */
    public function setFrom()
    {
        # do nothing...
    }

    /**
     * {@inheritdoc}
     */
    public function setDistance()
    {
        # do nothing...
    }

    /**
     * {@inheritdoc}
     */
    public function setDeadline()
    {
        # do nothing...
    }

    /**
     * {@inheritdoc}
     */
    public function setPegionId()
    {
        # do nothing...
    }

    /**
     * {@inheritdoc}
     */
    public function setCosts()
    {
        # do nothing...
    }

    /**
     * {@inheritdoc}
     */
    public function setStartsAt()
    {
        # do nothing...
    }

    /**
     * {@inheritdoc}
     */
    public function setEndsAt()
    {
        # do nothing...
    }

    /**
     * {@inheritdoc}
     */
    public function setDowntime()
    {
        # do nothing...
    }

    /**
     * {@inheritdoc}
     */
    public function setOverallEndsAt()
    {
        # do nothing...
    }

    /**
     * {@inheritdoc}
     */
    public function setCreatedAt()
    {
        # do nothing...
    }

    /**
     * {@inheritdoc}
     */
    public function setUpdatedAt()
    {
        # do nothing...
    }
}
