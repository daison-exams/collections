<?php

namespace Daison\Pegion\Repositories\Order;

use Daison\Pegion\Entities\Order;
use Daison\Pegion\Contracts\Order\CreateInterface;

class Create implements CreateInterface
{
    /**
     * {@inheritdoc}
     *
     * @throws
     */
    public function handle()
    {
        $order = Order::create([
            'distance'        => $this->distance,
            'deadline'        => $this->deadline,
            'starts_at'       => $this->startsAt,
            'ends_at'         => $this->endsAt,
            'pegion_id'       => $this->pegionId,
            'downtime'        => $this->downtime,
            'overall_ends_at' => $this->overallEndsAt,
            'costs'           => $this->costs,
        ]);

        return (new Details($order))->handle();
    }

    /**
     * {@inheritdoc}
     */
    public function setDistance($distance)
    {
        $this->distance = $distance;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setDeadline($deadline)
    {
        $this->deadline = $deadline;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setStartsAt($startsAt)
    {
        $this->startsAt = $startsAt;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setEndsAt($endsAt)
    {
        $this->endsAt = $endsAt;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setPegionId($pegionId)
    {
        $this->pegionId = $pegionId;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setDowntime($downtime)
    {
        $this->downtime = $downtime;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setOverallEndsAt($overallEndsAt)
    {
        $this->overallEndsAt = $overallEndsAt;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setCosts($costs)
    {
        $this->costs = $costs;

        return $this;
    }
}
