<?php

namespace Daison\Pegion\Repositories;

use Daison\Pegion\Entities\Pegion as Entity;
use Daison\Pegion\Contracts\PegionInterface;

class Pegion implements PegionInterface
{
    protected $pegion;

    public function __construct(Entity $pegion)
    {
        $this->pegion = $pegion;
    }

    /**
     * {@inheritdoc}
     */
    public function handle()
    {
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->pegion->name;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getSpeed()
    {
        return $this->pegion->speed;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getRange()
    {
        return $this->pegion->range;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getCost()
    {
        return $this->pegion->cost;
    }

    /**
     * {@inheritdoc}
     */
    public function getDowntime()
    {
        return $this->pegion->downtime;
    }

    /**
     * {@inheritdoc}
     */
    public function setName($name)
    {
        # do nothing...
    }

    /**
     * {@inheritdoc}
     */
    public function setSpeed($speed)
    {
        # do nothing...
    }

    /**
     * {@inheritdoc}
     */
    public function setRange($range)
    {
        # do nothing...
    }

    /**
     * {@inheritdoc}
     */
    public function setCost($cost)
    {
        # do nothing...
    }

    /**
     * {@inheritdoc}
     */
    public function setDowntime($downtime)
    {
        # do nothing...
    }
}
