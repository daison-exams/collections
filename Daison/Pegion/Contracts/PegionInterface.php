<?php

namespace Daison\Pegion\Contracts;

interface PegionInterface
{
    public function handle();

    public function setName($name);
    public function setSpeed($speed);
    public function setRange($range);
    public function setCost($cost);
    public function setDowntime($downtime);

    public function getName();
    public function getSpeed();
    public function getRange();
    public function getCost();
    public function getDowntime();
}
