<?php

namespace Daison\Pegion\Contracts\Order;

interface DetailsInterface
{
    public function handle();

    public function getId();
    public function getFrom();
    public function getDistance();
    public function getDeadline();
    public function getPegionId();
    public function getCosts();
    public function getStartsAt();
    public function getEndsAt();
    public function getDowntime();
    public function getOverallEndsAt();
    public function getCreatedAt();
    public function getUpdatedAt();

    public function setId();
    public function setFrom();
    public function setDistance();
    public function setDeadline();
    public function setPegionId();
    public function setCosts();
    public function setStartsAt();
    public function setEndsAt();
    public function setDowntime();
    public function setOverallEndsAt();
    public function setCreatedAt();
    public function setUpdatedAt();
}
