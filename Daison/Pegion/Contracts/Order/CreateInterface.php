<?php

namespace Daison\Pegion\Contracts\Order;

interface CreateInterface
{
    public function handle();
    public function setDistance($distance);
    public function setDeadline($deadline);
    public function setStartsAt($startsAt);
    public function setEndsAt($endsAt);
    public function setPegionId($pegionId);
    public function setDowntime($downtime);
    public function setOverallEndsAt($overallEndsAt);
    public function setCosts($costs);
}
