<?php

namespace Daison\Pegion\Contracts\Order;

interface ListsInterface
{
    public function handle();
    public function sortByAscending($field);
    public function currentPage($value);
    public function recordsPerPage($value);
}
