<?php

namespace Daison\Pegion\Database\Seeders;

use Illuminate\Database\Seeder;
use Daison\Pegion\Entities\Pegion;
use Illuminate\Database\Eloquent\Model;

class PegionDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        Pegion::updateOrCreate(
            ['name' => 'Antonion'],
            ['name' => 'Antonion',  'speed' => 70, 'range' => 600,  'cost' => 2, 'downtime' => 2]
        );
        Pegion::updateOrCreate(
            ['name' => 'Bonito'],
            ['name' => 'Bonito',    'speed' => 80, 'range' => 500,  'cost' => 2, 'downtime' => 3]
        );
        Pegion::updateOrCreate(
            ['name' => 'Carillo'],
            ['name' => 'Carillo',   'speed' => 65, 'range' => 1000, 'cost' => 2, 'downtime' => 3]
        );
        Pegion::updateOrCreate(
            ['name' => 'Alejandro'],
            ['name' => 'Alejandro', 'speed' => 70, 'range' => 800,  'cost' => 2, 'downtime' => 2]
        );
    }
}
