<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePegionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pegions', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name')->index()->unique();
            $table->decimal('speed', 8, 2);
            $table->string('range', 8, 2);
            $table->string('cost', 8, 2);
            $table->string('downtime', 8, 2);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pegions');
    }
}
