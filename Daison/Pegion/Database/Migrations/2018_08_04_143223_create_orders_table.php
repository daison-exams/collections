<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');

            $table->string('from')->index()->nullable();
            $table->decimal('distance');
            $table->datetime('deadline');
            $table->unsignedInteger('pegion_id')->index();
            $table->decimal('costs', 8, 2);
            $table->datetime('starts_at');
            $table->datetime('ends_at');
            $table->decimal('downtime', 8, 2);

            # we need this column, if we will force to use mysql in our database
            # we could totally remove this and use a raw sql query to combine ends_at + downtime
            # but sometimes I'm bored to try several database engines, since laravel's schema
            # is limited to to as it follows an Adapter pattern in 1 single database migration
            # schema.
            $table->datetime('overall_ends_at');

            $table->timestamps();
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->foreign('pegion_id')->references('id')->on('pegions')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
