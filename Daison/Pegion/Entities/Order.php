<?php

namespace Daison\Pegion\Entities;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * {@inheritDoc}
     */
    protected $fillable = [
        'distance',
        'deadline',

        'starts_at',
        'ends_at',
        'downtime',
        'overall_ends_at',
        
        # @TODO better to make this 'from' as 'user_id' and create a separate table which is 'users'
        'from',

        # even though we already referenced the pegion_id
        # it would be better to calculate the costs in the orders table itself
        # since a pegion's cost will change regulary, it's like in E-Commerce
        # where a product costs will always change!
        'pegion_id',
        'costs',
    ];

    /**
     * {@inheritDoc}
     */
    protected $casts = [
        'deadline' => 'datetime',
        'starts_at' => 'datetime',
        'ends_at' => 'datetime',
        'overall_ends_at' => 'datetime',
    ];

    /**
     * Get relations to Pegion.
     *
     * @return mixed
     */
    public function pegion()
    {
        return $this->belongsTo(Pegion::class);
    }
}
