<?php

namespace Daison\Pegion\Entities;

use Exception;
use Throwable;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Pegion extends Model
{
    /**
     * {@inheritDoc}
     */
    protected $fillable = [
        'name',
        'speed',
        'range',
        'cost',
        'downtime',
    ];

    /**
     * Get the hours based on provided distance.
     *
     * @param float $distance
     * @return void
     */
    public function getHoursByDistance(float $distance)
    {
        return bcdiv($distance, $this->speed, 2);
    }

    /**
     * Get the costs of the pegion per distance.
     *
     * @param float $distance
     * @return void
     */
    public function getCosts(float $distance)
    {
        return bcmul($this->cost, $distance, 2);
    }

    /**
     * Check if a pegion can reach the distance specified.
     *
     * @param float $distance
     * @return bool
     */
    public function isDistanceReachable(float $distance)
    {
        if ($distance <= $this->range) {
            return true;
        }

        return false;
    }

    /**
     * Calculate if the pegion is available the time specified.
     *
     * @return bool
     */
    public function isAvailable(float $distance, Carbon $deadline, $carbonNow = null)
    {
        try {
            $this->checkAvailability($distance, $deadline, $carbonNow);
        } catch (Throwable $e) {
            if (config('app.debug')) {
                logger($e->getMessage());
            }
            
            return false;
        }

        return true;
    }

    /**
     * Check the pegion's availability.
     *
     * This method will be useful for debugging which
     * part where the pegion is not available.
     *
     * @param float $distance
     * @param Carbon $deadline
     * @return void
     * @throws Exception
     */
    public function checkAvailability(float $distance, Carbon $deadline, $carbonNow = null)
    {
        $carbonNow = $carbonNow ?? Carbon::now();

        # first we need to check if the pegion can reach the distance specified
        if (! $this->isDistanceReachable($distance)) {
            throw new Exception('The distance is not reachable!');
        }

        # calculate the hours and subtract to comeup with deadline's earliest time.
        $hours = (float) $this->getHoursByDistance($distance);
        $deadlineEarliestTime = Carbon::parse($deadline)->subHours($hours);

        // dd([
        //     $carbonNow,
        //     $deadlineEarliestTime,
        //     $carbonNow->diffInMinutes($deadlineEarliestTime, false),
        // ]);

        # check the current time diff in
        if ($carbonNow->diffInMinutes($deadlineEarliestTime, false) < 0) {
            throw new Exception('The earliests time calculated is not possible!');
        }
        
        # we need to check this pegion's pending orders.
        $builder = (new Order)->newQuery()
            ->where('pegion_id', $this->id)
            ->where('overall_ends_at', '<=', $carbonNow)

            # now we need to compare the earliests time
            # versus the 2 dates
            ->whereBetween($deadlineEarliestTime, [
                DB::raw('starts_at'),
                DB::raw('overall_ends_at'),
            ]);

        if ($builder->count()) {
            throw new Exception('Currently busy the time specified!');
        }
    }

    /**
     * Get the earliests datetime where this pegion could start to work.
     *
     * @return Carbon
     */
    public function startDatetime($carbonNow = null)
    {
        $carbonNow = $carbonNow ?? Carbon::now();

        $previous = (new Order)->newQuery()
            ->where('pegion_id', $this->id)
            ->where('overall_ends_at', '>=', $carbonNow)
            ->orderBy('overall_ends_at', 'desc')
            ->first();

        if ($previous && $previous->overall_ends_at->diffInSeconds($carbonNow, false) < 0) {
            return $previous->overall_ends_at->addSecond();
        }

        return $carbonNow;
    }

    /**
     * Determine if the pegion has a downtime to this order.
     *
     * @param Requests\Orders\Add $request
     * @return bool
     */
    public function getDownTime()
    {
        $orders = Order::where('pegion_id', $this->id)
            ->orderBy('created_at', 'desc')
            ->limit($downtime = $this->downtime - 1)
            ->get();

        # if the current order's count is less than the downtime
        # thus, a fresh order for the pegion
        # and we could assume that we don't need yet a downtime
        if ($orders->count() < $downtime) {
            return 0;
        }

        # we need to check across all orders of this pegion
        # if it take a rest before, then we need to return false
        foreach ($orders as $order) {
            if ($order->downtime > 0) {
                return 0;
            }
        }

        return $this->downtime;
    }
}
