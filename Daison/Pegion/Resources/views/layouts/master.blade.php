<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>@yield('title')</title>

       {{-- Laravel Mix - CSS File --}}
       <link rel="stylesheet" href="{{ mix('css/pegion.css') }}">
    </head>
    <body>
        <div class="container-fluid">
            @yield('content')
        </div>

        {{-- Laravel Mix - JS File --}}
        <script src="{{ mix('js/pegion.js') }}"></script>
    </body>
</html>
