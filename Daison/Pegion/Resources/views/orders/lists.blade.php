@extends('pegion::layouts.master')

@section('title', 'Pegion: Lists of Orders')

@section('content')
<div id="content" class="row">
    <div class="col-md-10 mx-auto">
        <div class="row">
            <div class="col-md-3">
                @if (session()->has('errors'))
                    <div class="alert alert-danger">
                        <ul>
                            @foreach (session()->get('errors')->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <form method="post" action="{{ route('orders.add') }}">
                    @csrf()
                    <div class="form-group">
                        <label for="">Distance</label>
                        {{-- @TODO this  can be calculated if you will get the user's location along the destination, lol --}}
                        <input value="{{ old('distance') }}" type="text" placeholder="e.g 20" class="form-control" name="distance">
                    </div>
                    <div class="form-group">
                        <label for="">Deadline</label>
                        {{-- @TODO better to have a date & time picker --}}
                        <input value="{{ old('deadline') }}" type="text" placeholder="e.g 2018-08-04 15:30:00" class="form-control" name="deadline">
                    </div>
                    <div class="float-right">
                        <button class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
            <div class="col-md-9">
                {{-- <a href="" class="btn btn-danger">Delete All</a>
                <hr> --}}
                <h4>Orders</h4>
                <table class="table">
                    <tr>
                        <th>Pegion</th>
                        <th>Costs</th>
                        <th>Distance</th>
                        <th>Deadline</th>
                        <th>Starts At</th>
                        <th>Ends At</th>
                        <th>Calculated Time</th>
                        <th>Pegion Downtime (HR)</th>
                        <th>Pegion's Next Availability</th>
                    </tr>
                    @forelse ($orders as $order)
                    <tr>
                        <td>{{ $order['pegion']->getName() }}&nbsp;({{ $order['pegion']->getSpeed() }}/KM)</td>
                        <td>${{ number_format($order['costs'], 2) }}</td>
                        <td>{{ $order['distance'] }}KM</td>
                        <td>{{ Carbon\Carbon::parse($order['deadline'])->format($dtf = 'F d, Y g:iA') }}</td>
                        <td>{{ Carbon\Carbon::parse($order['starts_at'])->format($dtf) }}</td>
                        <td>{{ Carbon\Carbon::parse($order['ends_at'])->format($dtf) }}</td>
                        <td>{{ Carbon\Carbon::parse($order['starts_at'])->diffInHours($order['ends_at'], false) }}Hr</td>
                        <td>{{ $order['downtime'] }}</td>
                        <td>{{ Carbon\Carbon::parse($order['overall_ends_at'])->format($dtf) }}</td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="9" class="text-center">No orders found.</td>
                    </tr>
                    @endforelse
                </table>
                <div class="pagination justify-content-center">
                    {{ $orders->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@stop
