<?php

Route::group(['middleware' => 'web', 'domain' => config('domains.pegion'), 'namespace' => 'Daison\Pegion\Http\Controllers'], function () {
    Route::get('/', 'Orders\ListsController');
    Route::post('add', 'Orders\AddController')->name('orders.add');
});
