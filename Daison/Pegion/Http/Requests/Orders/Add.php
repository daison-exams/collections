<?php

namespace Daison\Pegion\Http\Requests\Orders;

use Carbon\Carbon;
use Daison\Pegion\Entities\Pegion;
use Illuminate\Foundation\Http\FormRequest;

class Add extends FormRequest
{
    /**
     * {@inheritDoc}
     */
    protected function validationData()
    {
        $this->pegion = null;
        $this->now = Carbon::now();

        foreach (Pegion::inRandomOrder()->get() as $pegion) {
            $isAvailable = $pegion->isAvailable(
                (float) $this->distance,
                Carbon::parse($this->deadline),
                $pegion->startDatetime($this->now)
            );

            if (! $isAvailable) {
                continue;
            }

            $this->pegion = $pegion;

            break;
        }

        return array_merge(
            parent::validationData(),
            [
                'has_pegion_available' => $this->pegion ? 1 : 0,
            ]
        );
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'distance'             => ['required'],
            'deadline'             => ['required', 'date'],
            'has_pegion_available' => ['required', 'in:1'],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function messages()
    {
        return [
            'has_pegion_available.in' => 'There are no available UberPegion at this moment',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
