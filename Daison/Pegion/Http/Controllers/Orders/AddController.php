<?php

namespace Daison\Pegion\Http\Controllers\Orders;

use Illuminate\Http\Response;
use Daison\Pegion\Http\Requests;
use Daison\Pegion\Entities\Order;
use Daison\Pegion\Repositories\Order\Create;
use Daison\Pegion\Http\Controllers\Controller;

class AddController extends Controller
{
    /**
     * Undocumented function
     *
     * @param Requests\Orders\Add $request
     * @return Response
     */
    public function __invoke(Requests\Orders\Add $request)
    {
        $repository = new Create();

        # @TODO would be better to separate the conditions under observers
        # for Single Responsibility and the calculators will only be specific
        # such as the ends_at.
        $order = $repository
            ->setDistance($request->distance)
            ->setDeadline($request->deadline)
            ->setStartsAt($startsAt = $request->pegion->startDatetime((clone $request->now)))
            ->setEndsAt(
                $endsAt = (clone $startsAt)->addHours(
                    $request->pegion->getHoursByDistance($request->distance)
                )
            )
            ->setPegionId($request->pegion->id)
            ->setDowntime($downtime = $request->pegion->getDownTime())
            ->setOverallEndsAt((clone $endsAt)->addHours($downtime))
            ->setCosts($request->pegion->getCosts($request->distance))
            ->handle();

        // dd($order);

        return redirect(url()->previous())
            ->withSuccess('You have successfully submitted an order!');
    }
}
