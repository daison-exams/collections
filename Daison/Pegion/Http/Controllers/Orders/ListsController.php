<?php

namespace Daison\Pegion\Http\Controllers\Orders;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Daison\Pegion\Entities\Order;
use Daison\Pegion\Repositories\Order\Lists;
use Daison\Pegion\Http\Controllers\Controller;
use Illuminate\Pagination\LengthAwarePaginator;

class ListsController extends Controller
{
    /**
     * Undocumented function
     *
     * @param Request $request
     * @return Response
     */
    public function __invoke(Request $request)
    {
        $builder = (new Order)->newQuery();

        $repository = new Lists($builder);
        $repository
            ->sortByAscending('pegion_id')
            ->sortByAscending('starts_at')
            ->currentPage($currentPage = $request->page)
            ->recordsPerPage($recordsPerPage = 5);

        # @TODO: add a filter function
        // foreach ($request->filters as $key => $val) {
        //     $repository->filter($request->filters);
        // }

        $orders = new LengthAwarePaginator(
            $repository->handle(),                      // items
            $repository->getLatestBuilder()->count(),   // total
            $recordsPerPage,                            // perPage
            $currentPage                                // currentPage
        );

        return $this->view('orders.lists', compact('orders'));
    }
}
