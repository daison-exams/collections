<?php

namespace Daison\Pegion\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    /**
     * Append the default module.
     *
     * @param string $path
     * @param array $params
     * @return mixed
     */
    protected function view($path, $params = [])
    {
        return view('pegion::'.$path, $params);
    }
}
