# Company

# Specifications

One day, out of the blue, your long-lost childhood friend asked you for help building a startup — an on-demand same-day traffic-independent document delivery service called

#### UberPigeon™. Here is its business logic:
- The customer submit an order containing distance and deadline
- UberPigeon™ determined and rejects the order if it is not possible to deliver before
the deadline.
- When the order is received, UberPigeon™ will calculate the cost for further invoicing
and save the order.

#### Each pigeon has its own attributes
- **Speed** (km/hr) The flying speed. The faster it fly, the less time it took to reach
destination.
- **Range** (km) The maximum distance it could fly in a trip
- **Cost** (fixed at $2/km)
- **Downtime** (hr) The time it needs to rest between two deliveries
Each delivery order contains
- **Distance (km) Trip distance
- **Deadline** (Datetime) The time it should reach the destination
Please design and build a minimum-viable API to support the operation of UberPigeon™
using a development framework of your choice. Please have in mind while developing that
you might need to add some future expansion to this service (Each pigeon has a maximum
weight it could carry, pigeon sick leave, for example …)

# Developer

## Requirements
- composer
- php 7.1+
- git

## Installation

```
git clone git@gitlab.com:daison-exams/collections.git Daison
cd Daison
composer install
cp .env .env.example
php artisan migrate
php artisan db:seed --class="Daison\Pegion\Database\Seeders\PegionDatabaseSeeder"
```

## Serve

```
php artisan serve
```